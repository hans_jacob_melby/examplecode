function Hello($scope, $http) {
    $http.get('/whoami').
        success(function(data) {
            $scope.greeting = data;
        });
}