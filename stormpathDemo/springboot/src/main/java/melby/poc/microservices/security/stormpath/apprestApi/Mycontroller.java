package melby.poc.microservices.security.stormpath.apprestApi;

import java.util.Enumeration;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.stormpath.sdk.account.Account;
import com.stormpath.sdk.directory.CustomData;
import com.stormpath.sdk.servlet.account.AccountResolver;

import melby.poc.microservices.security.config.ActiveMQConfig;


@Controller
@RequestMapping("/whoami")
public class Mycontroller {
	 @Autowired
	 private HttpServletRequest request;
	 
	 
	 @Autowired
	 private JmsTemplate jmstemplate;


	
	 @RequestMapping(method=RequestMethod.GET)
	 public @ResponseBody Greeting sayHello() {
	     Greeting greeting = new Greeting("Anonomus", "Hello Ananomus");
		 System.out.println(request.getAuthType());
		 Enumeration<String> headerNames = request.getHeaderNames();
		 while (headerNames.hasMoreElements()){
			 String nextElement = headerNames.nextElement();
			 String header = request.getHeader(nextElement);
			 System.out.println(nextElement+":"+header);
		 }
		 Cookie[] cookies = request.getCookies();
	     if (AccountResolver.INSTANCE.hasAccount(request)){
	    	 Account account = AccountResolver.INSTANCE.getRequiredAccount(request);
	    	 greeting = new Greeting(account.getUsername(), account.getFullName());
	    	 System.out.println(account.getHref());
	    	 System.out.println(account.getCustomData().size());
	    	 jmstemplate.convertAndSend(ActiveMQConfig.HELLO_QUEUE, "Hello from myController");
	    	 
	     }
		 
		 
		 
		 return  greeting;
	    }
}
