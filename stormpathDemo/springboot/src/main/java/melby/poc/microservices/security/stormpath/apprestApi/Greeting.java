package melby.poc.microservices.security.stormpath.apprestApi;

public class Greeting {

	String id = "Hello";
	String content = "World";
	public Greeting(String username, String string) {
		this.id=username;
		this.content=string;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
}
