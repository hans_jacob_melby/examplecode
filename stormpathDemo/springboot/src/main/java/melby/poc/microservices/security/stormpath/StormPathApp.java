package melby.poc.microservices.security.stormpath;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EnableJms
public class StormPathApp {

	public static void main(String[] args) {
		SpringApplication.run(StormPathApp.class, args);

	}

}
