package melby.poc.microservices.order.core.domain.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

import melby.poc.microservices.order.core.api.messaging.DomainEvent;
import melby.poc.microservices.order.core.api.messaging.IHandleDomainEvent;
import melby.poc.microservices.order.core.api.messaging.IHandleMessaging;


public class InMemmoryMessageBus implements IHandleMessaging{

	private static HashMap<String, List<IHandleDomainEvent<DomainEvent>>> observers = new HashMap<String, List<IHandleDomainEvent<DomainEvent>>>();
	private static HashMap<String, Queue > queues = new HashMap<String,Queue>();
	
	public  void publishEvent(DomainEvent event) {
		List<IHandleDomainEvent<DomainEvent>> list = observers.get(event.getClass().getName());
		if (list != null) {
			list.stream().forEach(r -> r.handle(event));
		}
	}
	
	

	public void subscribeToEvent(Class clazz, IHandleDomainEvent<DomainEvent> handler) {
		List<IHandleDomainEvent<DomainEvent>> list = observers.get(clazz.getName());
		if(list==null){
			list = new ArrayList<IHandleDomainEvent<DomainEvent>>();
		}
		list.add(handler);
		observers.put(clazz.getName(), list);
		

	}

	@Override
	public void send(String queuename, Object message) {
		Queue queue = queues.get(queuename);
		if (queue==null){
			queue = new ConcurrentLinkedQueue<Object>();
			queues.put(queuename, queue);
		}
		queue.add(message);
		
		
	}

}
