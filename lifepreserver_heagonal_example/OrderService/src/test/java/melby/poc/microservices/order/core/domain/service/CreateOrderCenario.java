package melby.poc.microservices.order.core.domain.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import melby.poc.microservices.order.core.adapters.storage.InMemmoryEventStore;
import melby.poc.microservices.order.core.api.OrderAcceptedEvent;
import melby.poc.microservices.order.core.api.OrderRejectedEvent;
import melby.poc.microservices.order.core.api.exceptions.OrderRejectedException;
import melby.poc.microservices.order.core.api.messaging.DomainEvent;
import melby.poc.microservices.order.core.domain.service.createOrderCommandHandler;
import melby.poc.microservices.order.storage.Order;

public class CreateOrderCenario {

	InMemmoryEventStore inmemEventStore;
	Order order = new Order("1");

	@Before
	public void setUp() throws Exception {
		inmemEventStore = new InMemmoryEventStore(new InMemmoryMessageBus());

	}

	

	/*
	 * Given  A Valid order is Reveived an Event of type ValidOrderReceivedorderAccepted will be published
	 * */	
	@Test
	public void GivenValidOrderReceivedorderAcceptedWillBepublished() {
		given(null);
		Order order = new Order("1");
		
		createOrderCommandHandler handler = new createOrderCommandHandler(inmemEventStore,order);
		handler.handle();
		List<DomainEvent> events = inmemEventStore.getEvents("1");
		assertEquals(1, events.size());
		assertTrue(OrderAcceptedEvent.class.getName().equalsIgnoreCase(events.get(0).getClass().getName()));

	}
	
	
	/*
	 * Given  A user has ordered product 
	 * When the order is canseled 
	 * Then the user should get a refund
	 * 
	 * */	
	@Test
	public void RefundPolicyTest1() {
		given(new OrderAcceptedEvent(new Order("1")));
		Order order = new Order("1");
		
		RefundCommandHandler handler = new RefundCommandHandler(inmemEventStore,order);
		handler.handle();
		List<DomainEvent> events = inmemEventStore.getEvents("1");
		assertEquals(1, events.size());
		assertTrue(OrderAcceptedEvent.class.getName().equalsIgnoreCase(events.get(0).getClass().getName()));

	}

	@Test
	public void GivenInValidOrderReceived_orderOrderRejectedException_WillBeThrown() {
		given(null);
		Order order = new Order();
		createOrderCommandHandler handler = new createOrderCommandHandler(inmemEventStore,order);
		
		
		try {
			handler.handle();
			fail("Expected OrderRejectedException not thrown");
		} catch (OrderRejectedException e) {

		}
		List<DomainEvent> events = inmemEventStore.getEvents("1");
		assertEquals(0, events.size());

	}

	@Test
	public void Giventest() {

		given(new OrderAcceptedEvent(order), new OrderRejectedEvent(order));

		List<DomainEvent> events = inmemEventStore.getEvents("1");
		assertEquals(2, events.size());
		assertTrue(OrderAcceptedEvent.class.getName().equalsIgnoreCase(events.get(0).getClass().getName()));

	}

	private void given(DomainEvent... domainevents) {
		inmemEventStore.clear();
		if (domainevents != null) {
			for (int i = 0; i < domainevents.length; i++) {
				inmemEventStore.save(domainevents[i]);
			}
		}

	}

}
