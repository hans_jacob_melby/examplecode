package melby.poc.microservices.order;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import melby.poc.microservices.order.core.api.messaging.DomainEvent;
import melby.poc.microservices.order.core.api.messaging.IHandleMessaging;
import melby.poc.microservices.order.core.ports.storage.OrderEventRepository;


public class InMemmoryEventStore implements OrderEventRepository {
	
	@Autowired
	public InMemmoryEventStore(IHandleMessaging bus) {
		
		this.bus = bus;
	}

	
	public static HashMap<String,List<DomainEvent>> eventstore = new HashMap<String,List<DomainEvent>>();
	
	private IHandleMessaging bus;
	
	public void save(DomainEvent event) {
		List<DomainEvent> list = eventstore.get(event.getAgregateId());
		if(list==null){
			list = new ArrayList<DomainEvent>();
			
		}
		list.add(event);
		eventstore.put(event.getAgregateId(), list);
		bus.publishEvent(event);
		
		
	}

	
	public List<DomainEvent> getEvents(String agregateId) {
		List<DomainEvent> response = new ArrayList<DomainEvent>();
		List<DomainEvent> list = eventstore.get(agregateId);
		if(list!=null){
			response.addAll(list);	//should really clone these objects to prevent modification.
		}
		return response;
	}
	
	public void clear(){
		eventstore.clear();
		
	}
	

}
