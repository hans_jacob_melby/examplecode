package melby.poc.microservices.order.core.ports.storage;

import java.util.List;

import melby.poc.microservices.order.core.api.messaging.DomainEvent;

public interface OrderEventRepository {

	public void save(DomainEvent event);
	public List<DomainEvent> getEvents(String agregateId);
}
