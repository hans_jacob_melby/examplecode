package melby.poc.microservices.order.core.api.exceptions;

import javax.validation.ValidationException;

public class OrderRejectedException extends ValidationException {
	public OrderRejectedException(String message) {
		super(message);
	}

	private static final long serialVersionUID = 8963918077873323741L;
	
}
