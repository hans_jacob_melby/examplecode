package melby.poc.microservices.order.core.api.messaging;

import java.util.Date;
import java.util.UUID;

public class DomainEvent {
	private String eventId;
	private String agregateId;
	private Date eventDate;
	private Date receivedDate;
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public String getAgregateId() {
		return agregateId;
	}
	public void setAgregateId(String agregateId) {
		this.agregateId = agregateId;
	}
	public DomainEvent(String agregateId, Date eventDate) {
		eventId = UUID.randomUUID().toString();
		this.eventId = eventId;
		if (eventDate==null){
			this.eventDate=new Date();
		}
		this.eventDate=eventDate;
		
		this.agregateId = agregateId;
		
	}
	
	
}
