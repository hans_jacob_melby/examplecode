package melby.poc.microservices.order.jms;

import java.util.List;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import melby.poc.microservices.order.core.api.messaging.DomainEvent;
import melby.poc.microservices.order.core.domain.saga.OrderPolicy;
import melby.poc.microservices.order.core.ports.storage.OrderEventRepository;


@Component
public class LineItemShippedEventHandler {

	OrderEventRepository repo;
	
	@JmsListener(destination = "melby.poc.microservices.order.core.api.LineItemShippedEven")
	public void receiveOrder(DomainEvent event) {
		List<DomainEvent> events = repo.getEvents(event.getAgregateId());
		OrderPolicy order = new OrderPolicy();
		order.playEventStream(events);
		order.handle(event);
	}
	
	

}
