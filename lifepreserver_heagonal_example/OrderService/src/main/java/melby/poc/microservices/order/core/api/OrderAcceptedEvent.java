package melby.poc.microservices.order.core.api;

import java.util.Date;

import melby.poc.microservices.order.core.api.messaging.DomainEvent;
import melby.poc.microservices.order.storage.Order;

public class OrderAcceptedEvent extends DomainEvent{
	public Order order;

	public OrderAcceptedEvent(Order order){
		super(order.getCustomerID(),new Date());
		
		this.order=order;
	}
	public Order getOrder(){
		return order;
	}
}
