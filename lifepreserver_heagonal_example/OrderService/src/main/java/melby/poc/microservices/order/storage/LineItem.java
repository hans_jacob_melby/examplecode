package melby.poc.microservices.order.storage;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class LineItem {
	@Id
	@GeneratedValue
	public Long lineItemId;
	public String productName;
	public double price;
	public String status;
	
}
