package melby.poc.microservices.order.core.adapters.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import melby.poc.microservices.order.core.api.messaging.DomainEvent;
import melby.poc.microservices.order.core.api.messaging.IHandleMessaging;

@Component
public  class ActiveMQAdapter implements IHandleMessaging {
	
	JmsTemplate jmsTemplate;

	

	@Override
	public void publishEvent(DomainEvent event) {
		 
		jmsTemplate.setPubSubDomain(true);
		jmsTemplate.convertAndSend(event.getClass().getName(), event);

	}

	@Autowired
	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}
	
	@Override
	public void send(String queue, Object message) { 
		jmsTemplate.setPubSubDomain(false);
		jmsTemplate.convertAndSend(queue, message);

	}
	
	
	
	
	
	
	
}
