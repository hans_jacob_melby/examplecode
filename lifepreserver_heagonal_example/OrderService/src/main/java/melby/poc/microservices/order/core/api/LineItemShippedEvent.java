package melby.poc.microservices.order.core.api;

import java.util.Date;

import melby.poc.microservices.order.core.api.messaging.DomainEvent;

public class LineItemShippedEvent extends DomainEvent{
	
	private String lineItemId;
	private String orderId;

	public LineItemShippedEvent(String _orderId,String _lineItemId){
		super(_orderId,new Date());
		super.setAgregateId(_orderId);
		this.orderId = _orderId;
		this.lineItemId = _lineItemId;
	}
	public String getLineItemId() {
		return lineItemId;
	}

	public String getOrderId() {
		return orderId;
	}

	
	
	
}
