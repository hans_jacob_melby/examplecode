package melby.poc.microservices.order.core.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import melby.poc.microservices.order.core.ports.storage.OrderEventRepository;
import melby.poc.microservices.order.storage.Order;

@Component
public class OrderService  {

	private OrderEventRepository repository;

	@Autowired
	public OrderService(OrderEventRepository repository) {
		this.repository = repository;
	}

	public void createOrder(Order order) {
		createOrderCommandHandler handler = new createOrderCommandHandler(repository, order);
		handler.handle();
	}

}
