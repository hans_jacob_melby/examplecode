	package melby.poc.microservices.order.core.domain.saga;

import java.util.List;

import melby.poc.microservices.order.core.api.LineItemShippedEvent;
import melby.poc.microservices.order.core.api.OrderAcceptedEvent;
import melby.poc.microservices.order.core.api.messaging.DomainEvent;
import melby.poc.microservices.order.storage.LineItem;

public class OrderPolicy {
	public long orderID;
	private List<LineItem> orderlines;
	private String CustomerID;
	private String orderStatus;
	
	public void handle(DomainEvent event){
		if (event instanceof OrderAcceptedEvent){
			handle((OrderAcceptedEvent)event);
		}
		if (event instanceof LineItemShippedEvent){
			handle((LineItemShippedEvent)event);
		}
	}
	
	private void handle(OrderAcceptedEvent event){
		this.orderID=event.getOrder().getOrderId();
		this.orderlines = event.getOrder().getOrderlines();
		this.CustomerID = event.getAgregateId();
		this.orderStatus="ACCEPTED";
	}
	
	private void handle(LineItemShippedEvent event){
		for(LineItem item:orderlines){
			if(item.lineItemId==Long.valueOf(event.getLineItemId())){
				item.status="SHIPPED";
				orderStatus="PARTIAL_SHIPPED";
			}
		}
		if(isallLineItemsShipped()){
			this.orderStatus="ALL_SHIPPED";
		}
			
	}
	private boolean isallLineItemsShipped(){
		for(LineItem item:orderlines){
			if(!"SHIPPED".equalsIgnoreCase(item.status)){
				return false;
			}
		}
		return true;
	}
	
	public void playEventStream(List<DomainEvent> events){
		events.stream().forEachOrdered(r->handle(r));
	}

	public String getOrderStatus(){
		return orderStatus;
	}
	
}
