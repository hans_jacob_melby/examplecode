package melby.poc.microservices.order.core.api;

import java.util.Date;

import melby.poc.microservices.order.core.api.messaging.DomainEvent;
import melby.poc.microservices.order.storage.Order;

public class OrderRejectedEvent extends DomainEvent{
		public Order order;

		public OrderRejectedEvent(Order order){
			super(order.getCustomerID(),new Date());
			this.order=order;
		}

}
