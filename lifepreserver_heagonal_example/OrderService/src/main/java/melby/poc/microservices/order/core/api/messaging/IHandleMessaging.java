package melby.poc.microservices.order.core.api.messaging;

public interface IHandleMessaging {

	public void publishEvent(DomainEvent event);
	public void send(String queue, Object message);
}
