package melby.poc.microservices.order.core.ports.storage;

import org.springframework.data.jpa.repository.JpaRepository;

import melby.poc.microservices.order.storage.Order;




public interface OrderRepository extends JpaRepository<Order, Long>{
	
}
