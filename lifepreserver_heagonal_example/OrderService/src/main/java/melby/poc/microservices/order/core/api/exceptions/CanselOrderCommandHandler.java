package melby.poc.microservices.order.core.api.exceptions;

import melby.poc.microservices.order.core.api.OrderAcceptedEvent;
import melby.poc.microservices.order.core.api.IHandleCommands;
import melby.poc.microservices.order.core.domain.saga.OrderPolicy;
import melby.poc.microservices.order.core.ports.storage.OrderEventRepository;
import melby.poc.microservices.order.storage.Order;

public class CanselOrderCommandHandler implements IHandleCommands {

	OrderEventRepository repository;
	private String customerID;
	private String orderId;

	public CanselOrderCommandHandler(OrderEventRepository repository, String _customerID,String _orderId) {
		this.repository = repository;
		customerID =_customerID;
		orderId  =_orderId;
		
	}

	

	@Override
	public void handle() {
		
		OrderPolicy order = new OrderPolicy();
		order.playEventStream(repository.getEvents(orderId));

	}
	
	public void CanselOrder(Order order) throws CanselationRejectedException{
		
	}



}
