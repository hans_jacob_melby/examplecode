package melby.poc.microservices.order.core.api.messaging;

public interface IHandleDomainEvent<T extends DomainEvent> {

	public void handle(T t);
	
}
