package melby.poc.microservices.order.storage;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;


@Entity
public class Order {
	@Id
	@GeneratedValue
	private Long orderId;
	@OneToMany
	private List<LineItem> orderlines;
	private String CustomerID;
	private String orderStatus;
	
	public Order(){
		
	}
	
	public Order(String customerID){
		this.CustomerID =customerID;
	}

	public List<LineItem> getOrderlines() {
		return orderlines;
	}

	public void setOrderlines(List<LineItem> orderlines) {
		this.orderlines = orderlines;
	}

	public Long getOrderId() {
		return orderId;
	}

	public String getCustomerID() {
		return CustomerID;
	}

	public String getOrderStatus() {
		return orderStatus;
	}
	
	
	
	
	
	
}
