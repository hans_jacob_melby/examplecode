package melby.poc.microservices.order.core.domain.service;

import melby.poc.microservices.order.core.api.OrderAcceptedEvent;
import melby.poc.microservices.order.core.api.IHandleCommands;
import melby.poc.microservices.order.core.api.exceptions.OrderRejectedException;
import melby.poc.microservices.order.core.ports.storage.OrderEventRepository;
import melby.poc.microservices.order.storage.Order;

class createOrderCommandHandler implements IHandleCommands {

	OrderEventRepository repository;
	Order order;

	public createOrderCommandHandler(OrderEventRepository repository, Order order) {
		this.repository = repository;
		this.order = order;
	}

	private void validateOrder(Order order) throws OrderRejectedException {
		if (order == null) {
			throw new OrderRejectedException("Order must be present");
		}
		if (order.getCustomerID() == null) {
			throw new OrderRejectedException("order must include CustomerID");
		}

	}

	@Override
	public void handle() {
		createOrder(order);

	}

	private void createOrder(Order order) throws OrderRejectedException {

		validateOrder(order);
		repository.save(new OrderAcceptedEvent(order));
	}

}
