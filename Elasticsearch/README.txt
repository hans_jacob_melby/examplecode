https://www.elastic.co/guide/en/elasticsearch/reference/current/_exploring_your_data.html

to create an index that is in memmory only (great for test purposes)

curl -XPUT "http://localhost:9200/my_index/" -d'
{
    "settings": {
        "index.store.type": "memory"
    }
}'


To install marvel 
run bin/plugin -i elasticsearch/marvel/latest


Maven depdendency for pure java api
dependency>
    <groupId>org.elasticsearch</groupId>
    <artifactId>elasticsearch</artifactId>
    <version>${es.version}</version>
</dependency>


Simple CRUD
CREATE

General info

PUT request to http://localhost:9200/<index>/<type>/[<id>]
include a JSON object as the PUT data.

Index and type are required while the id part is optional. If we don�t specify an ID ElasticSearch
will generate one for us. However, if we don�t specify an id we should use POST instead of PUT

The index name is arbitrary. If there isn�t an index with that name on the server already one will
be created using default configuration.
As for the type name it too is arbitrary. It serves several purposes, including:
� Each type has its own ID space.
� Different types can have different mappings (�schema� that defines how properties/fields
should be indexed).
� Although it�s possible, and common, to search over multiple types, it�s easy to search only
for one or more specific type(s).

Create example :
curl -XPUT "http://localhost:9200/movies/movie/1" -d'
{
"title": "The Godfather",
"director": "Francis Ford Coppola",
"year": 1972
}'

UPDATE example
curl -XPUT "http://localhost:9200/movies/movie/1" -d'
{
"title": "The Godfather",
"director": "Francis Ford Coppola",
"year": 1972,
"genres": ["Crime", "Drama"]
}'
Notice version  an created atribute in response :
{
"_version": 2,
"created": false
}
if we were to delete the document the version number wouldn�t be reset meaning that if we later
indexed a document with the same ID the version number would be greater than one.

If we supply a version in indexing requests ElasticSearch will then only overwrite the document
if the supplied version is the same as for the document in the index.
Example :

curl -XPUT "http://localhost:9200/movies/movie/1?version=1" -d'
{
"title": "The Godfather"
}'
response : 
{
"error": "VersionConflictEngineException[[movies][2] [movie][1]: version conflict, cu\
rrent [2], provided [1]]",
"status": 409
}
indexing didn�t happen due to a version conflict.

Get by ID
http://localhost:9200/<index>/<type>/<id>

example
curl -XGET "http://localhost:9200/movies/movie/1"

response : 
{
"_index": "movies",
"_type": "movie",
"_id": "1",
"_version": 2,
"found": true,
"_source": {
	"title": "The Godfather",
	"director": "Francis Ford Coppola",
	"year": 1972,
	"genres": [
	  "Crime",
	  "Drama"
	  ]
	}
}

DELETE
curl -XDELETE "http://localhost:9200/movies/movie/1"
Response 
{
"found": true,
"_index": "movies",
"_type": "movie",
"_id": "1",
"_version": 3
}





Why dows this not work :
PUT /movies/movie/1
{
"title": "The Godfather",
"director": {
   "givenName": "Francis Ford",
    "surName": "Coppola",
     "awards": [
         {
      "name": "Oscar",
      "type": "Director",
      "year": 1974,
      "movie": "The Godfather Part II"
      }
      ]
},
"year": 1972
}



curl -XPOST "http://localhost:9200/movies/_search" -d'
{
"query": {
"query_string": {
"query": "Francis Ford Coppola"
}
}
}'

is interpited as Francis OR Ford OR Coppola
To "fix" this use the follwing synta

curl -XPOST "http://localhost:9200/movies/_search" -d'
{
"query": {
"query_string": {
"query": "Francis Ford Coppola",
"default_operator": "AND"
}
}
}'

For exact mathc (string) this can be used
POST /movies/_search
{
"query": {
"query_string": {
"query": "\"Francis Ford Coppola\""
}
}
}


Bulk inserts
curl -XPUT localhost:9200/<index>   //creates the index

curl -XPOST localhost:9200/<index>/<type>/_bulk --data-binary @file.ejson   //note that file need to be formated correctly. Plese see example files in test data directory

curl -XPOST localhost:9200/<index>/<type>/_mapping  //shows all mappings that elasticsearch is doing (out of the box)

mapping is "equal" to scheema
how we can query them
how we can search them
dont but all your bets in doing it right the first time.
dont use elasticsearch as your primary data storage!!!!
you will need to reindex several times


Query DSL
match
{
"query" : {YOUR QUERY HERE}
}

{
	"query" : {
				"match" : {"text_entry" : "romeo"}
			}
}
multimatch
{
	"query" : {
				"multi_match" : {
					"query" : "romeo",
					"fields": ["text_entry","speaker"]
				}
				
				
			}
}

Bool //must match test enty romeo but not speaker ROMEO. Should match speaker JULIET OR match speaker FRIAR LAURENCE
{
	"query" : {
				"bool" : {
					"must" : {"match":{"text_entry":"romeo"}},
					"must_not" : {"match":{"speaker":"ROMEO"}},
					"should" : [
					{"match":{"speaker":"JULIET"}},
					{"match":{"speaker":"FRIAR LAURENCE"}}
					]
				}
				
				
			}
}

Filtering
Filters are fatser than queries (they dont score)
filters can be cached in memmory 
rule : if relevance is not important, use filters, otherwize use queries

if posible use filters first and query on the result of the filter.

The filtered query
{
  "query" : {
	"filtered" : {
	  "query" : {"match" : {"content" : "abcde"}},
	  "filter" : {"term" : {"tag" : "awesome"}}
	}
  }
}


Visaalization
Kibana
D3 if you want to create your own..
tutorial : https://www.elastic.co/blog/data-visualization-elasticsearch-aggregations
http://www.fullscale.co/blog/2013/03/20/getting_started_with_elasticsearch_and_AngularJS_d3.html
http://www.fullscale.co/blog/2013/03/07/getting_started_with_elasticsearch_and-AngularJS_faceting.html
http://www.fullscale.co/blog/2013/02/28/getting_started_with_elasticsearch_and_AngularJS_searching.html

Highcharts


Aggregations
combination of buckets and metrics
select count(color) //metrics
from table
group by color //buckets

{
  "aggs" : {
   "speakers" : { //aggregatoion name
     "terms" : {  //bucket type
	   "field" :"speaker"
	   }
	 }
  }
}








