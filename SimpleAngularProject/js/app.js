var app = angular.module('app', []);

app.controller('scNavBarCtrl', function ($scope) {

});
app.directive('navBar', function () {
	return {
		restrict: 'E',
		templateUrl: 'partials/navbar/navbar.html',
		controller: 'scNavBarCtrl'
	};
});

app.controller('scNavBarLoginCtrl', function ($scope, notifier) {
	$scope.signin = function (name, password) {
		if (name === 'Harry' && password === 'Ginny') {
			notifier.notify('You have successfully signed in!');
			$scope.authenticated = true;
			$scope.username = "hansj.melby@gmail.com";
		} else {
			notifier.notify('Username/Password Combination incorrect');
		}
	};
	$scope.signout = function () {
		$scope.authenticated = false;
		$scope.username = null;
		$scope.password = null;
		notifier.notify('You have successfully signed out!');
	};
});
app.directive('navBarLogin', function () {
	return {
		restrict: 'E',
		templateUrl: 'partials/navbar/navbarLogin.html',
		controller: 'scNavBarLoginCtrl'
	};
});

app.directive('news', function () {
	return {
		restrict: 'E',
		templateUrl: 'partials/news/news.html',
		controller: 'newsController',	
	};
});


app.controller('newsController', ['$scope', 'notifier', 'news','$sce',function ($scope, notifier, news,$sce) {
	$scope.news = news._embedded["rh:doc"];
	
	for (i = 0; i < $scope.news.length; i++) { 
    
	$scope.news[i].safeHtml= $sce.trustAsHtml($scope.news[i].heading);
	console.log($scope.news[i]);
	}
	$scope.input = $sce.trustAsHtml('<input></input>');


}]);

app.filter("sanitize", ['$sce', function($sce) {
  return function(htmlCode){
    return $sce.trustAsHtml(htmlCode);
  }
}]);

app.filter('unsafe', function($sce) { return $sce.trustAsHtml; });

app.directive('schedule', function () {
	return {
		restrict: 'E',
		templateUrl: 'partials/schedule.html',
		controller: 'scNyhetCtrl',
		scope: {
			catalog: '='
		}
	};
});

app.directive('userProfileInformation', function () {
	return {
		restrict: 'E',
		templateUrl: 'partials/navbar/userProfile.html',
		controller: 'scNavBarLoginCtrl'
	};
});

app.factory('notifier', function (toastr) {
	return {
		notify: function (msg) {
			toastr.success(msg);
			console.log(msg);
		},
		info: function (msg) {
			toastr.info(msg);
			console.log(msg);
		}
	};
});