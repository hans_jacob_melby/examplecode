app.value('toastr', toastr);
app.value('subscriptions', [
  {id: 1, name: 'Nyheter'},
  {id: 2, name: 'Styret Informerer'},
  {id: 3, name: 'Dugnader'}
]);

app.value('catalog', [
  {id:1, name: "Styret informerer", credits: 3, instructorName: 'Professor Trelawney', registered: false},
  {id:2, name: 'Dugnad', credits: 5, instructorName: 'Professor Hooch', registered: false},
  {id:3, name: 'Generelle nyheter', credits: 2, instructorName: 'Professor Grubbly-Plank', registered: false},
  {id:4, name: 'Generalforsamling', credits: 4, instructorName: 'Professor Lockhart', registered: true},
  {id:5, name: 'Markeringer', credits: 1, instructorName: 'Professor Lupin', registered: false},
]);

app.value('nyheter', [
  {id:1, Overskrift: "Styret informerer", dato: 3, HTML: 'Professor Trelawney'},
  {id:2, Overskrift: 'Dugnad', dato: 5, HTML: 'Professor Hooch'},
  {id:3, Overskrift: 'Generelle nyheter', dato: 2, HTML: 'Professor Grubbly-Plank'},
  {id:4, Overskrift: 'Generalforsamling', dato: 4, HTML: 'Professor Lockhart'},
  {id:5, Overskrift: 'Markeringer', dato: 1, HTML: 'Professor Lupin'},
]);

app.value('hendelser', [{id:1,Dato:"21.01.1975",beskrivelse:"dette er en event"}]);

app.value('news',{
	"_embedded": {
		"rh:doc": [{
			"_embedded": {},
			"_links": {
				"self": {
					"href": "/ghf/news/5566eb5e18375a2e6f9e620b"
				},
				"rh:coll": {
					"href": "/ghf"
				},
				"curies": [{
					"href": "http://www.restheart.org/docs/v0.10/#api-doc-{rel}",
					"name": "rh"
				}]
			},
			"_type": "DOCUMENT",
			"_id": {
				"$oid": "5566eb5e18375a2e6f9e620b"
			},
			"author": "hansj.melby@gmail.com",
			"heading": "<h1>dette er h1</h1>",
			"_created_on": "2015-05-28T10:18:06Z"
		}]
	},
	"_links": {
		"self": {
			"href": "/ghf/news"
		},
		"rh:db": {
			"href": "/ghf"
		},
		"rh:filter": {
			"href": "/ghf/news/{?filter}",
			"templated": true
		},
		"rh:sort": {
			"href": "/ghf/news/{?sort_by}",
			"templated": true
		},
		"rh:paging": {
			"href": "/ghf/news/{?page}{&pagesize}",
			"templated": true
		},
		"rh:countandpaging": {
			"href": "/ghf/news/{?page}{&pagesize}&count",
			"templated": true
		},
		"rh:indexes": {
			"href": "/ghf/news/_indexes"
		},
		"curies": [{
			"href": "http://www.restheart.org/docs/v0.10/#api-coll-{rel}",
			"templated": true,
			"name": "rh"
		}]
	},
	"_type": "COLLECTION",
	"_id": "news",
	"_created_on": "2015-05-28T10:25:41Z",
	"_etag": {
		"$oid": "5566ed251837c4b4eeceacde"
	},
	"_lastupdated_on": "2015-05-28T10:25:41Z",
	"_collection-props-cached": false,
	"_returned": 1
});