package com.example.configclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class SpringconfigclientPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringconfigclientPocApplication.class, args);
	}

    @RefreshScope
    @RestController
    class MessageRestController {

        @Autowired
        Environment env;

        @Value("${message:Hello default}")
        private String message;

        @RequestMapping("/message")
        String getMessage() {
            return this.message;
        }
        @RequestMapping("/connect/{by}")
        String connect(@PathVariable("by") String by) {
            return env.getProperty("jdbc."+by+".url");
        }
    }
}
